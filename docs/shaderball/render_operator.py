
import bpy
from bpy.props import IntProperty, FloatProperty
from pathlib import Path
import copy

OUTPUT = Path.home() / "Downloads" / "manual_images"
PREFIX = "render_shader-nodes_shader_"
FORMAT = ".webp"
FRAMES = 5
COLLECTION = "Shader Balls"

tmp_dir = OUTPUT / "tmp"

def render_frame(object, material, frame):
    name = material.name
    scene = bpy.context.scene
    prev_material = object.material_slots[0].material
    
    # Only make this object visible.
    objects = [ob for ob in bpy.data.collections[COLLECTION].all_objects]
    for other_object in objects:
        other_object.hide_render = True
    object.hide_render = False

    # Setup scene to render animation.
    object.material_slots[0].material = material
    scene.render.filepath = str(tmp_dir / (name + "#"))
    scene.use_nodes = False
    
    # Render frame and load into image for compositing.
    scene.frame_start = frame
    scene.frame_end = frame
    bpy.ops.render.render(animation=True)

    image = bpy.data.images["render" + str(frame)]
    image.filepath = str(tmp_dir / (name + str(frame) + ".exr"))
    image.reload()
    
    # Restore state
    object.material_slots[0].material = prev_material
    scene.frame_start = 1
    scene.frame_end = FRAMES
    scene.frame_current = 1

def render_composite(object, material):
    name = material.name
    scene = bpy.context.scene
        
    # Composite frames into one image.
    scene.use_nodes = True
    file_output = scene.node_tree.nodes["File Output"]
    file_output.base_path = str(OUTPUT)
    file_output.file_slots[0].path = name + "#"
    scene.render.filepath = str(tmp_dir / name)
    
    # Render and rename to final name.
    bpy.ops.render.render()
    from_path = OUTPUT / (name + "1" + FORMAT)
    to_path = OUTPUT / (PREFIX + name + FORMAT)
    from_path.rename(to_path)
    
    # Restore state.
    scene.use_nodes = False

def setup(all=False):
    scene = bpy.context.scene
    scene.frame_current = 1
    objects = [ob for ob in bpy.data.collections[COLLECTION].all_objects]

    # Image that everything will be composited into.
    base_image = bpy.data.images["base"]
    base_image.generated_width = scene.render.resolution_x * FRAMES
    base_image.generated_height = scene.render.resolution_y
    
    jobs = []

    if all:
        for object in objects:
            render_slot = False
            for slot in object.material_slots[1:]:
                # Skip slots before space in the list.
                if not slot.material:
                    render_slot = True
                    continue
                if not render_slot:
                    continue
                
                # Make copy to avoid lambda late binding issues.
                o = object
                m = slot.material
                for f in range(1, FRAMES + 1):
                    jobs.append(lambda o=o, m=m, f=f: render_frame(o, m, f))
                jobs.append(lambda o=o, m=m: render_composite(o, m))
    else:
        for object in objects:
            if not object.hide_viewport:
                o = object
                m = object.material_slots[0].material
                for f in range(1, FRAMES + 1):
                    jobs.append(lambda o=o, m=m, f=f: render_frame(o, m, f))
                jobs.append(lambda o=o, m=m: render_composite(o, m))   

    return jobs        

# Operators
class BaseOperator:
    _timer = None
    _jobs = []

    def modal(self, context, event): 
        if event.type in {'RIGHTMOUSE', 'ESC'}:
            self.cancel(context)
            return {'CANCELLED'}

        if event.type == 'TIMER':
            context.window.cursor_set("WAIT")
            job = self._jobs.pop(0)
            job()
            context.window.cursor_set("DEFAULT")
            if len(self._jobs) == 0:
                return {'FINISHED'}

        return {'PASS_THROUGH'}

    def execute(self, context):
        wm = context.window_manager
        self._timer = wm.event_timer_add(0.01, window=context.window)
        self._jobs = setup(self.all)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

class RenderManualImageOperator(BaseOperator, bpy.types.Operator):
    bl_idname = "render.manual_image"
    bl_label = "Render Manual Image"    
    all = False
        
class RenderAllManualImagesOperator(BaseOperator, bpy.types.Operator):
    bl_idname = "render.all_manual_images"
    bl_label = "Render Manual Images (All)"
    all = True

def register():
    bpy.utils.register_class(RenderManualImageOperator)
    bpy.utils.register_class(RenderAllManualImagesOperator)

def unregister():
    bpy.utils.register_class(RenderManualImageOperator)
    bpy.utils.unregister_class(RenderAllManualImagesOperator)

if __name__ == "__main__":
    register()
